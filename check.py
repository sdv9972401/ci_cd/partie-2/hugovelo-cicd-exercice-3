import json
import argparse
import sys

# Set up command-line argument parsing
parser = argparse.ArgumentParser(description='Process JSON data to extract Information Disclosure - Suspicious Comments alerts.')
parser.add_argument('json_file', type=str, help='Path to the JSON file')
args = parser.parse_args()

# Read JSON data from the specified file
with open(args.json_file, 'r') as file:
    data = file.read()

json_data = json.loads(data)
target_alert = "Information Disclosure - Suspicious Comments"
alert_found = False

for site in json_data["site"]:
    for alert in site["alerts"]:
        if alert['alert'] == target_alert:
            alert_found = True
            print(f"Alert: {alert['alert']}\nRisk: {alert['riskdesc']}\n")
            break

if alert_found:
    print(f"Found '{target_alert}' in website's configuration. Please review and update accordingly.")
    sys.exit(1)  # Exit with an error code if target alert found
else:
    print(f"No '{target_alert}' alerts detected.")