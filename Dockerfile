# Base image
FROM alpine:latest as builder

# Setup working directory
WORKDIR app/

# Install Hugo and Git
RUN apk add --no-cache --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community hugo && \
    apk add --no-cache git

# Copy website
COPY . .

# Clone the theme
RUN git submodule add --force https://github.com/theNewDynamic/gohugo-theme-ananke.git velohugo/themes/ananke

# Setup workdir again
WORKDIR /app/velohugo

# Build the website
RUN mkdir -p /app/velohugo/public && \
    hugo -d /app/velohugo/public

# Add vulnerability
#RUN echo "<!-- TODO: Remove Password: Username: admin, password: FZkdzpoikfze -->" >> /app/velohugo/public/index.html

# Base image
FROM nginx:alpine

# Setup working directory
WORKDIR /usr/share/nginx/html

# Copy rendered website
COPY --from=builder app/velohugo/public /usr/share/nginx/html

# Expose port
EXPOSE 80